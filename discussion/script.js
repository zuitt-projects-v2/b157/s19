console.log("Hello World");

//Exponent Operator
/*
	An exponent operator has been added to simplify the calculation for the exponent of a given number

*/


const firstNum = Math.pow(8, 2);
console.log(firstNum); //64

const secondNum = 8 ** 2;
console.log(secondNum);

const thirdNum = 5 ** 3;
console.log(thirdNum); //125

//Template Literals
/*
	It allows us to write strings without using operator(+)
*/

let name = "Garry";

//Pre-Template Literal String
let message = 'Hello ' + name + '! Welcome to programming!'
console.log(message);

//Strings Using Template Literal
//Uses the backticks(``)
/*
	`` - template literal
	[] - array literal
	{} - object literals
	${} - placeholder

*/
message = `Hello ${name}! Welcome to programming!`;
console.log(message)

//Multi-line Using Template Literals
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.

`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings is: ${principal * interestRate}`);


//Array Destructuring
/*
	Allow us to unpack elements in arrays into distinct variables.
	-It allow us to name array elements with variables instead of using the index number.

	Syntax: 
		let/const [variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"];

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}. It's nice to see you!`)

//Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
//console.log(anotherName);

//Object Destructuring
/*
	Allow us to unpack properties of objects into distinct variables.
	It shortens the syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName} = object;

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

//Object Destructuring
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullName({givenName, maidenName, familyName}){
	console.log(`${ givenName} ${maidenName} ${familyName}`)
}

getFullName(person)


//Arrow Functions
/*
	-Compact alternative syntax to traditional functions
	-Useful for code snippets where creating a function will not be reused in any other portion of the code

	Syntax:
		const variableName = () => {
			console.log()
		}

*/

const hello = () => {
	console.log("Hello World");
}

hello()

//Pre-arrow Function and Template Literals
/*	
	-Traditional Function
	Syntax:
		function functionName (parameterA, parameterB){
			console.log()
		}

*/

/*function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + ' ' + middleInitial + ' ' + lastName)
}

printFullName("John", "D.", "Smith");*/

//Arrow Function
/*	
	Syntax:
		let/const variableName = (parameterA, parameterB) => {
				console.log();
		}
*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("Mark", "D.", "Smith")

//Another example
//Pre-arrow function
const students = ["Corazon", "Luwalhati", "Maria"];
students.forEach(
			function(student){
				console.log(`${student} is a student.`)
			}
	)

//Arrow Function
students.forEach( (student) => {
	console.log(`${student} is a student.`)
})

//Implicit return statement
/*
	There are instances when you can omit the "return" statement 

*/

//Pre-Arrow Function
/*function add(x, y) {
	return x + y
}

const add = (x, y) => {
	return x + y
}


let total = add(1,2)
console.log(total)*/

//Arrow Function
const add = (x, y) => x + y;
let total = add(1,2)
console.log(total);

//Default function argument value

const greet = (name = 'User') => {
	return `Good morning, ${name}!`
}

console.log(greet())
console.log(greet("John"))

//Class-based Object Blueprint
/*
	Allows creation/ instantiation of objects using classes as blueprints
	
	The Constructor is a special method for a class for creating/ initializing an object for that class

	Syntax: 
		class className {
			constructor(objectProperyA, objectPropertyB){
				this.objectProperyA = objectProperyA;
				this.objectProperyB = objectProperyB;
			}
		}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car()
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar)

const myNewCar = new Car("BMW", "Hot Wheels", 2021)
console.log(myNewCar)
