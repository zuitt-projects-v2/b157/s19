// Activity:
// 1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the script.js file to the index.html file.



// 13. Create a git repository named S19.
// 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 15. Add the link in Boodle.


// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

class Cube {
	constructor (_side) {
		this.side = _side
	}

	get volume() {
		// 4. Using Template Literals, print out the value of the getCube variable with a message of 
		// The cube of <num> is…
		return `The cube of ${this.side} is ${this.side ** 3}`
	}
}

let getCube = new Cube(3)
console.log(getCube.volume)

// 5. Create a variable address with a value of an array containing details of an address.
let address = ['Little Baguio Terraces', 'N. Domingo Street', 'San Juan', 'Metro Manila', 'Philippines']
// 6. Destructure the array and print out a message with the full address using Template Literals.
const [bldg, street, purok, province, city] = address
console.log(`I live at ${bldg}, ${street}, ${purok}, ${province}, ${city}`)
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
let animal = {
	name: 'Nygel',
	breed: 'Dog',
	color: 'Brown',
	age: 3
}
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const {name, breed, color, age} = animal
console.log(`${name} is a ${breed}. She is ${age} years old and her color is ${color}`)

// 9. Create an array of numbers.
let arrayNumbers = Array.from(Array(10), (_,x) => x)
console.log(arrayNumbers)

arrayNumbers.forEach( e => console.log(e))

// 11. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog {
	constructor (_name, _age, _breed) {
		this.name = _name,
		this.age = _age,
		this.breed = _breed
	}
}

// 12. Create/instantiate a new object from the class Dog and console log the object.
const newDog = new Dog('Boy', 16, 'Shih Tzu')
console.log(newDog)
